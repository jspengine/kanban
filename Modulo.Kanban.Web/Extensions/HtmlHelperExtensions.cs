﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Modulo.Kanban.Web.Components;

namespace Modulo.Kanban.Web.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString ComplexDropDownList(this HtmlHelper html,
            string fieldName, ComplexSelectList selectList, string optionLabel,
            object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromStringExpression(fieldName, html.ViewData);
            return RenderSelect(html, metadata, selectList, fieldName, optionLabel, htmlAttributes, false);
        }

        public static MvcHtmlString ComplexDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, ComplexSelectList selectList, string optionLabel, object htmlAttributes)
        {
            var fieldName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            return RenderSelect(html, metadata, selectList, fieldName, optionLabel, htmlAttributes, false);
        }

        public static MvcHtmlString HierarchicalDropDownList(this HtmlHelper html,
            string fieldName, ComplexSelectList selectList, string optionLabel,
            object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromStringExpression(fieldName, html.ViewData);
            return RenderSelect(html, metadata, selectList, fieldName, optionLabel, htmlAttributes, true);
        }

        public static MvcHtmlString HierarchicalDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, ComplexSelectList selectList, string optionLabel, object htmlAttributes)
        {
            var fieldName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            return RenderSelect(html, metadata, selectList, fieldName, optionLabel, htmlAttributes, true);
        }

        private static MvcHtmlString RenderSelect(HtmlHelper htmlHelper, ModelMetadata metadata, ComplexSelectList selectList, string fieldName, string optionLabel, object htmlAttributes, bool hierarquical)
        {

            TagBuilder tag = new TagBuilder("select");
            tag.MergeAttribute("id", fieldName);
            tag.MergeAttribute("name", fieldName);
            tag.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            tag.MergeAttributes(htmlHelper.GetUnobtrusiveValidationAttributes(fieldName, metadata));
            if (optionLabel != null)
            {
                TagBuilder option = new TagBuilder("option");
                option.MergeAttribute("value", string.Empty);
                option.InnerHtml = optionLabel;

                tag.InnerHtml += option.ToString();
            }

            RenderOptions(tag, selectList, 0, false, hierarquical);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        private static void RenderOptions(TagBuilder tag, IEnumerable<ComplexSelectListItem> items, int level, bool last, bool hierarquical)
        {
            if (items != null && items.Any())
            {
                int i = 0;
                foreach (var selectListItem in items)
                {
                    TagBuilder option = new TagBuilder("option");
                    option.MergeAttribute("value", selectListItem.Value);
                    //option.InnerHtml = selectListItem.Text;

                    if (hierarquical)
                    {
                        for (int k = 0; k < level; k++)
                        {
                            if (k > 0 && last) option.InnerHtml += "&nbsp;";
                            else if (k > 0 && selectListItem.ParentItem != null) option.InnerHtml += "│";
                            option.InnerHtml += "&nbsp;&nbsp;&nbsp;";
                        }

                        if (level > 0)
                            if (i == items.Count() - 1)
                                option.InnerHtml += "└──";
                            else option.InnerHtml += "├──";

                        option.InnerHtml += "&nbsp;";
                    }
                    option.InnerHtml += string.IsNullOrEmpty(selectListItem.Text) ? "." : selectListItem.Text;

                    if (selectListItem.Attributes != null && selectListItem.Attributes.Count > 0)
                    {
                        foreach (var attribute in selectListItem.Attributes)
                        {
                            option.MergeAttribute(attribute.Key, attribute.Value);
                        }
                    }

                    if (selectListItem.Selected)
                        option.MergeAttribute("selected", "selected");

                    tag.InnerHtml += option.ToString();

                    if (hierarquical && selectListItem.SubItems.Any())
                        RenderOptions(tag, selectListItem.SubItems, level + 1, i == items.Count() - 1, hierarquical);
                    i++;
                }
            }
        }
    }
}