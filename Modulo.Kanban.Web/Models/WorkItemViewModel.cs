﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Modulo.Kanban.Domain.Entities;

namespace Modulo.Kanban.Web.Models
{
    public class KanbanViewModel
    {
        public string TaskAssignedTo { get; set; }
        public ProjectViewModel CurrentProject { get; set; }
        public IterationViewModel CurrentIteration { get; set; }

        [Display(Name = "Team")]
        public string CurrentTeam { get; set; }
        public List<UserStoryViewModel> UserStories { get; set; }
        public List<IterationViewModel> Iterations { get; set; }
        public List<string> Teams { get; set; }
        public List<Sprint> Sprints { get; set; }
        public KanbanViewModel()
        {
            UserStories = new List<UserStoryViewModel>();
            Iterations = new List<IterationViewModel>();
            Teams = new List<string>();
        }
    }

    public class ProjectViewModel
    {
        [Display(Name = "Project")]
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class IterationViewModel
    {
        [Display(Name = "Iteration")]
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public abstract class WorkItemViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string State { get; set; }
        public string Description { get; set; }
        public UserViewModel AssignedTo { get; set; }
    }
    public class TaskViewModel : WorkItemViewModel
    {
        
    }

    public class UserStoryViewModel : WorkItemViewModel
    {
        public List<TaskViewModel> Tasks { get; set; }

        public UserStoryViewModel()
        {
            Tasks = new List<TaskViewModel>();
        }
    }

    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public Guid? ImageId { get; set; }
    }
}