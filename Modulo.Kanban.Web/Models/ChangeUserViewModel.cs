﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Modulo.Kanban.Web.Models
{
    public class ChangeUserViewModel
    {
        [Display(Name = "Current user")]
        public UserViewModel CurrentUser { get; set; }

        [Display(Name = "To user")]
        public UserViewModel ToUser { get; set; }

        public List<UserViewModel> Users { get; set; }

        public ChangeUserViewModel()
        {
            Users = new List<UserViewModel>();
        }
    }
}