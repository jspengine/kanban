﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.Practices.Unity;
using Modulo.Kanban.Domain.Contracts;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.ServiceAgent;
using Modulo.Kanban.Web.Models;

namespace Modulo.Kanban.Web.Controllers
{
    public class BaseController : Controller
    {
        [Dependency]
        protected IKanbanService KanbanService { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewBag.Projects = Mapper.Map<List<Project>, List<ProjectViewModel>>(KanbanService.ListProjects());
            base.OnActionExecuting(filterContext);
        }
    }
}