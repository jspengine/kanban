﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Web.Models;

namespace Modulo.Kanban.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var model = Mapper.Map<List<Project>, List<ProjectViewModel>>(KanbanService.ListProjects());
            return View(model);
        }
    }
}