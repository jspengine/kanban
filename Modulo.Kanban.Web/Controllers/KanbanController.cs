﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Web.Components;
using Modulo.Kanban.Web.Models;

namespace Modulo.Kanban.Web.Controllers
{
    public class KanbanController : BaseController
    {
        public KanbanController()
        {
        }

        public ActionResult Index(string projectName)
        {
            var model = new KanbanViewModel();
            model.Teams = KanbanService.ListTeams();
            model.Sprints = KanbanService.ListSprints(projectName);
            model.CurrentProject =
                Mapper.Map<Domain.Entities.Project, ProjectViewModel>(
                    KanbanService.GetProjectByName(projectName));

            int? iterationId = null;
            var currentSprint = KanbanService.GetCurrentSprint(projectName);
            if (currentSprint != null) iterationId = currentSprint.Id;
            ViewBag.Iterations = GetIterationListItems(model.CurrentProject.Id, iterationId);
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(string projectName, KanbanViewModel model)
        {
            var collection = KanbanService.ListUserStories(projectName, model.CurrentIteration.Id, model.CurrentTeam);
            model.Teams = KanbanService.ListTeams();

            var date = DateTime.Now;
            var sprint = KanbanService.GetCurrentSprint(projectName);
            if (model.CurrentIteration.Id > 0)
            {
                sprint = KanbanService.GetSprint(projectName, model.CurrentIteration.Id);
                if (sprint != null && sprint.StartDate.HasValue && sprint.EndDate.HasValue)
                {
                    var days = sprint.EndDate.Value - sprint.StartDate.Value;
                    date = sprint.StartDate.Value.AddDays(days.TotalDays / 2);
                }
            }
            model.Sprints = KanbanService.ListSprints(projectName, sprint.ReleaseId, date);

            model.CurrentProject =
                Mapper.Map<Domain.Entities.Project, ProjectViewModel>(
                    KanbanService.GetProjectByName(projectName));

            foreach (UserStory item in collection)
            {
                var userStory = Mapper.Map<UserStory, UserStoryViewModel>(item);
                var tasks = KanbanService.ListTasks(projectName, item.Id, model.TaskAssignedTo);
                userStory.Tasks = Mapper.Map<List<UserStoryTask>, List<TaskViewModel>>(tasks);
                model.UserStories.Add(userStory);
            }

            ViewBag.Iterations = GetIterationListItems(model.CurrentProject.Id, model.CurrentIteration.Id);

            return View(model);
        }

        public PartialViewResult UserStories(string projectName, int id, string teamName)
        {
            var collection = Mapper.Map<List<UserStory>, List<UserStoryViewModel>>(KanbanService.ListUserStories(projectName, id, teamName));
            return PartialView("_UserStories", collection);
        }

        public ActionResult Task(int? id)
        {
            TaskViewModel model = new TaskViewModel();
            if (id.HasValue)
            {
                model = Mapper.Map<UserStoryTask, TaskViewModel>(KanbanService.GetTask(id.Value));
            }
            return View("TaskForm", model);
        }

        private ComplexSelectList GetIterationListItems(int projectId, int? iterationId)
        {
            var iterations = new List<ComplexSelectListItem>();

            if (projectId > 0)
            {
                var nodes = KanbanService.ListIterations(projectId);

                if (nodes != null)
                    LoadIterations(iterations, nodes, iterationId);
            }
            return new ComplexSelectList(iterations);
        }

        [HttpPost]
        public PartialViewResult Iterations(int id)
        {
            var model = new KanbanViewModel();
            model.CurrentProject = new ProjectViewModel { Id = id };
            ViewBag.Iterations = GetIterationListItems(model.CurrentProject.Id, null);

            return PartialView("_Iterations", model);
        }

        [HttpPost]
        public JsonResult ChangeTaskStatus(int id, string status)
        {
            bool success = false;
            string errorMessage = string.Empty;
            var state = UserStoryTaskState.ToDo;

            if (Enum.TryParse(status, true, out state))
            {
                try
                {
                    KanbanService.UpdateTaskStatus(id, state);
                    success = true;
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }
            else
            {
                errorMessage = "Invalid task state";
            }
            return Json(new { success, errorMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeTestCaseStatus(int id, string status)
        {
            bool success = false;
            string errorMessage = string.Empty;
            var state = UserStoryTestCaseState.ToTest;

            if (Enum.TryParse(status, true, out state))
            {
                try
                {
                    KanbanService.UpdateTestCaseStatus(id, state);
                    success = true;
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }
            else
            {
                errorMessage = "Invalid test case state";
            }
            return Json(new { success, errorMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeUserStoryStatus(int id, string status)
        {
            bool success = false;
            string errorMessage = string.Empty;
            var state = UserStoryState.Available;

            if (Enum.TryParse(status, true, out state))
            {
                try
                {
                    KanbanService.UpdateUserStoryStatus(id, state);
                    success = true;
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }
            else
            {
                errorMessage = "Invalid user story state";
            }
            return Json(new { success, errorMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public PartialViewResult ChangeTaskUser(int id)
        {
            var task = KanbanService.GetTask(id);
            var model = new ChangeUserViewModel();
            model.CurrentUser = Mapper.Map<User, UserViewModel>(task.AssignedTo);
            model.Users = Mapper.Map<List<User>, List<UserViewModel>>(KanbanService.ListUsers(task.ProjectName));
            return PartialView("ChangeUserForm", model);
        }

        [HttpPost]
        public JsonResult ChangeUser(int id, Guid userId)
        {
            return Json(new {}, JsonRequestBehavior.AllowGet);
        }

        private void LoadIterations(List<ComplexSelectListItem> list, List<Iteration> iterations, int? iterationId)
        {
            foreach (Iteration node in iterations)
            {
                var model = new ComplexSelectListItem();
                model.Value = node.Id.ToString();
                model.Text = node.Name;
                
                if (iterationId.HasValue && node.Id == iterationId.Value)
                    model.Selected = true;

                if (node.SubIterations.Any()) LoadIterations(model.SubItems, node.SubIterations, iterationId);

                list.Add(model);
            }
        }
    }
}