﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;

namespace Modulo.Kanban.Web.Components
{
    public class ComplexSelectListOptions
    {
        public string DataValueField { get; set; }
        public string DataTextField { get; set; }

        public string DataAttributesField { get; set; }
        public string DataSubItemsField { get; set; }

        public object SelectedValue { get; set; }
    }
    public class ComplexSelectList : IEnumerable<ComplexSelectListItem>
    {
        public IEnumerable Items { get; private set; }
        public ComplexSelectListOptions Options { get; private set; }

        public ComplexSelectList(IEnumerable items)
            : this(items, null /* selectedValue */)
        {
        }

        public ComplexSelectList(IEnumerable items, object selectedValue)
            : this(items, null /* dataValuefield */, null /* dataTextField */, selectedValue)
        {
        }

        public ComplexSelectList(IEnumerable items, string dataValueField, string dataTextField)
            : this(items, dataValueField, dataTextField, null /* selectedValue */)
        {
        }

        public ComplexSelectList(IEnumerable items, string dataValueField, string dataTextField, object selectedValue)
            : this(items, new ComplexSelectListOptions
            {
                DataValueField = dataTextField,
                DataTextField = dataTextField,
                SelectedValue = selectedValue
            })
        {
            
        }

        public ComplexSelectList(IEnumerable items, ComplexSelectListOptions options)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            Items = items;
            Options = options ?? new ComplexSelectListOptions();
        }


        private static IEnumerable ToEnumerable(object selectedValue)
        {
            return (selectedValue != null) ? new object[] { selectedValue } : null;
        }

        internal IList<ComplexSelectListItem> GetListItems()
        {
            var selectedValue = string.Empty;
            if (Options.SelectedValue != null)
                selectedValue = Convert.ToString(Options.SelectedValue, CultureInfo.CurrentCulture);

            IList<ComplexSelectListItem> listItems = new List<ComplexSelectListItem>();
            foreach (var item in Items)
            {
                if (item.GetType().IsAssignableFrom(typeof(ComplexSelectListItem)))
                    listItems.Add((ComplexSelectListItem)item);
                else
                {
                    var listItem = new ComplexSelectListItem();
                    if (!String.IsNullOrEmpty(Options.DataValueField))
                    {
                        listItem.Value = Eval(item, Options.DataValueField);
                        listItem.Selected = (Options.SelectedValue != null && selectedValue == listItem.Value);
                    }
                    else listItem.Selected = (item == Options.SelectedValue);
                    listItem.Text = Eval(item, Options.DataTextField);

                    listItems.Add(listItem);
                }
            }
            return listItems.ToList();
        }

        private static string Eval(object container, string expression)
        {
            object value = container;
            if (!String.IsNullOrEmpty(expression))
            {
                value = DataBinder.Eval(container, expression);
            }
            return Convert.ToString(value, CultureInfo.CurrentCulture);
        }

        public IEnumerator<ComplexSelectListItem> GetEnumerator()
        {
            return GetListItems().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
