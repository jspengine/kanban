﻿using System.Collections.Generic;

namespace Modulo.Kanban.Web.Components
{
    public class ComplexSelectListItem
    {
        public bool Selected { get; set; }

        public string Text { get; set; }

        public string Value { get; set; }

        public IDictionary<string, string> Attributes { get; set; }

        public ComplexSelectListItem ParentItem { get; set; }
        public List<ComplexSelectListItem> SubItems { get; set; }

        public ComplexSelectListItem()
        {
            SubItems = new List<ComplexSelectListItem>();
        }
    }
}
