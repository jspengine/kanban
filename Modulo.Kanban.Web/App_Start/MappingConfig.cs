﻿using AutoMapper;
using Modulo.Kanban.Web.Mappers;

namespace Modulo.Kanban.Web
{
    public class MappingConfig
    {
        public static void Configure()
        {
            Mapper.AddProfile(new ProjectMapperProfile());

            Mapper.AddProfile(new UserMapperProfile());
            Mapper.AddProfile(new UserStoryMapperProfile());
            Mapper.AddProfile(new UserStoryTaskMapperProfile());
        }
    }
}