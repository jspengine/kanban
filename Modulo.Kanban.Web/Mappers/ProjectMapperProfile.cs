﻿using AutoMapper;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Web.Models;

namespace Modulo.Kanban.Web.Mappers
{
    public class ProjectMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Project, ProjectViewModel>()
                .ForMember(o => o.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(o => o.Name, opt => opt.MapFrom(src => src.Name))
                
                .ReverseMap();
        }
    }
}
