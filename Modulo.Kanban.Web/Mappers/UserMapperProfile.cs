﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Web.Models;

namespace Modulo.Kanban.Web.Mappers
{
    public class UserMapperProfile : Profile
    {
        public UserMapperProfile()
        {
            Mapper.CreateMap<User, UserViewModel>()
                .ForMember(o => o.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(o => o.DisplayName, opt => opt.MapFrom(src => src.DisplayName))
                .ForMember(o => o.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(o => o.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(o => o.ImageId, opt => opt.MapFrom(src => src.ImageId))
                .ReverseMap();
        }
        
    }
}