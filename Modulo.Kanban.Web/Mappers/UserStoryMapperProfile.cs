﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Web.Models;

namespace Modulo.Kanban.Web.Mappers
{
    public class UserStoryMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UserStory, UserStoryViewModel>()
                .ForMember(o => o.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(o => o.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(o => o.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(o => o.State, opt => opt.MapFrom(src => src.State))
                .ForMember(o => o.AssignedTo, opt => opt.MapFrom(src => src.AssignedTo))

                .ForMember(o => o.Tasks, opt => opt.MapFrom(src => src.Tasks))

                .ReverseMap();
        }
    }
}