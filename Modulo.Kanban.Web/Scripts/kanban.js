﻿$(function () {
    var hash = window.location.hash;
    hash && $('ul.nav-hash a[href="' + hash + '"]').tab('show');

    $('.nav-hash a').click(function (e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });

    $(".step-content .panel-body").dotdotdot();

    $("#accordion-userstories").collapse().on("show.bs.collapse", function (e) {
        setTimeout(function() {
            $(".step-content .panel-body").dotdotdot();
        }, 500);
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        //e.target // activated tab
        //e.relatedTarget // previous tab
        $(".step-content .panel-body").dotdotdot();
    });

    var sortableContents = $(".step-content");
    sortableContents.sortable({
        connectWith: ".step-content",
        stop: function (event, ui) {
            var item = $(ui.item);
            var step = item.parents(".step");
            var content = step.parent();

            $.ajax({
                type: "POST",
                url: content.data("changestatusurl") + "/" + item.data("id"),
                async: false,
                data: { status: step.data("state")}
            }).done(function(data) {
                if (!data.success) {
                    sortableContents.sortable('cancel');
                } else {
                    item.removeClass("panel-info panel-warning panel-success");
                    item.addClass("panel-" + step.data("color"));
                }
            }).fail(function() {
                sortableContents.sortable('cancel');
            });
        },
        revert: true
    }).disableSelection();

    $("#ddl-projects").change(function () {
        var projects = $(this);
        var url = projects.data("iterationsurl");
        $.post(url + "/" + projects.val(), null, function (html) {
            var sel = $(html);
            $(".ddl-iterations").html(sel.html());
        });
    });

    $(".btn-newtask").click(function(e) {
        e.preventDefault();
        var url = $(this).attr("href");

        $.ajax({
            type: "GET",
            url: url,
            dataType: "html"
        }).done(function(html) {
            $("body").append(html);

            $(".modal").modal().on('hidden.bs.modal', function (e) {
                $(this).remove();
            });
        });
    });

    $(".lnk-changeuser").click(function (e) {
        e.preventDefault();
        var url = $(this).attr("href");

        $.ajax({
            type: "GET",
            url: url,
            dataType: "html"
        }).done(function (html) {
            $("body").append(html);

            $(".modal").modal().on('hidden.bs.modal', function (e) {
                $(this).remove();
            });
        });
    });
});
$(window).resize(function () {
    $(".step-content .panel-body").dotdotdot();
});