﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modulo.Kanban.Domain.Exceptions
{
    public class UpdateWorkItemException : Exception
    {
        public UpdateWorkItemException() : base()
        {
            
        }
        public UpdateWorkItemException(string message)
            : base(message)
        {

        }

        public UpdateWorkItemException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public UpdateWorkItemException(Exception innerException) : base(innerException.Message, innerException)
        {
            
        }
    }
}
