﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modulo.Kanban.Domain.Entities
{
    public class Iteration
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Iteration> SubIterations { get; set; }

        public Iteration()
        {
            SubIterations = new List<Iteration>();
        }
    }
}
