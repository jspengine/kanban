﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modulo.Kanban.Domain.Entities
{
    public enum UserStoryState
    {
        Available,
        Accepted,
        Delivered,
        Rejected
    }

    public class UserStory : BaseWorkItem
    {
        public virtual ICollection<UserStoryTask> Tasks { get; set; }
    }
}
