﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modulo.Kanban.Domain.Entities
{
    public abstract class BaseWorkItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string State { get; set; }
        public string Description { get; set; }
        public User AssignedTo { get; set; }
        public string ProjectName { get; set; }
        protected BaseWorkItem()
        {
            AssignedTo = new User();
        }
    }
}
