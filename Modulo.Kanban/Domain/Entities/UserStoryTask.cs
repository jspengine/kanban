﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modulo.Kanban.Domain.Entities
{
    public enum UserStoryTaskState
    {
        ToDo,
        Doing,
        Done
    }

    public class UserStoryTask : BaseWorkItem
    {

    }
}
