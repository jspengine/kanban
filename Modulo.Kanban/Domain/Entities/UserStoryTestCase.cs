﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modulo.Kanban.Domain.Entities
{
    public enum UserStoryTestCaseState
    {
        ToTest,
        Testing,
        Tested
    }

    public class UserStoryTestCase : BaseWorkItem
    {
    }
}
