﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modulo.Kanban.Framework.Repository;

namespace Modulo.Kanban.Domain.Entities
{
    public class State : IEntity
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string ReferenceName { get; set; }
        public uint Position { get; set; }
    }
}
