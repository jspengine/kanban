﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modulo.Kanban.Domain.Entities;

namespace Modulo.Kanban.Domain.Contracts
{
    public interface IKanbanService
    {
        Project GetProjectByName(string projectName);

        List<User> ListUsers(string projectName);

        List<string> ListTeams();
        List<Iteration> ListIterations(int projectId);

        List<Project> ListProjects();

        #region Sprint
        List<Sprint> ListSprints(string projectName);
        List<Sprint> ListSprints(string projectName, int? releaseId, DateTime date);

        Sprint GetSprint(string projectName, int sprintId);
        Sprint GetCurrentSprint(string projectName);
        Sprint GetSprintByDate(string projectName, DateTime date, int? releaseId = null);
        #endregion

        #region Task
        List<UserStoryTask> ListTasks(string projectName, string userName);
        List<UserStoryTask> ListTasks(string projectName, int userStoryId);
        List<UserStoryTask> ListTasks(string projectName, int userStoryId, string userName);
        UserStoryTask GetTask(int taskId);
        void UpdateTask(UserStoryTask task);
        void UpdateTaskStatus(int taskId, UserStoryTaskState state);
        void AddTask(int projectId, UserStoryTask task);
        #endregion

        #region Test Case
        List<UserStoryTestCase> ListTestCases(string projectName, string userName);
        List<UserStoryTestCase> ListTestCases(string projectName, int userStoryId);
        List<UserStoryTestCase> ListTestCases(string projectName, int userStoryId, string userName);
        UserStoryTestCase GetTestCase(int testCaseId);
        void UpdateTestCaseStatus(int testCaseId, UserStoryTestCaseState state);
        #endregion

        List<UserStory> ListUserStories(string projectName, int iterationId, string teamName, bool withTasks = false);
        void UpdateUserStoryStatus(int userStoryId, UserStoryState state);
    }
}
