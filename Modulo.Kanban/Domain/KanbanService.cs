﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Microsoft.Practices.Unity;
using Microsoft.TeamFoundation.Framework.Client;
using Microsoft.TeamFoundation.Framework.Common;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Modulo.Kanban.Domain.Contracts;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Domain.Exceptions;
using Modulo.Kanban.Framework.Specification;
using Modulo.Kanban.Integration.Repository.Contracts;
using Modulo.Kanban.ServiceAgent;
using Project = Modulo.Kanban.Domain.Entities.Project;

namespace Modulo.Kanban.Domain
{
    public class KanbanService : IKanbanService
    {
        [Dependency]
        protected TfsServiceAgent TfsService { get; set; }

        [Dependency]
        protected IStateRepository StateRepository { get; set; }

        public KanbanService()
        {
            
        }

        public List<User> ListUsers(string projectName)
        {
            var users = TfsService.ListIdentities(projectName);
            return users.Select(TeamFoundationIdentityToUser).ToList();
        }

        public Project GetProjectByName(string projectName)
        {
            Project result = null;
            var project = TfsService.GetProjectByName(projectName);

            if (project != null)
            {
                result =new Project();
                result.Id = project.Id;
                result.Name = project.Name;
                result.Description = "";
            }
            return result;
        }

        public List<string> ListTeams()
        {
            return TfsService.ListFieldValues("NG.Team").ToList();
        }

        public List<Iteration> ListIterations(int projectId)
        {
            var iterations = new List<Iteration>();

            if (projectId > 0)
            {
                var nodes = TfsService.ListIterations(projectId);

                if (nodes != null)
                    LoadIterations(iterations, nodes);
            }

            return iterations;
        }

        public List<Sprint> ListSprints(string projectName, int? releaseId, DateTime date)
        {
            var result = new List<Sprint>();
            var current = GetSprintByDate(projectName, date, releaseId);
            if (current != null)
            {
                if (current.StartDate.HasValue)
                {
                    var prevDate = current.StartDate.Value.AddDays(-7);
                    var prev = GetSprintByDate(projectName, prevDate, current.ReleaseId);

                    if (prev != null) result.Add(prev);
                }

                result.Add(current);

                if (current.EndDate.HasValue)
                {
                    var nextDate = current.EndDate.Value.AddDays(7);
                    var next = GetSprintByDate(projectName, nextDate, current.ReleaseId);

                    if (next != null) result.Add(next);
                }
            }
            return result;
        }

        public List<Sprint> ListSprints(string projectName)
        {
            return ListSprints(projectName, null, DateTime.Now);
        }

        public Sprint GetSprint(string projectName, int sprintId)
        {
            var result = new Sprint();
            var node = TfsService.GetIteration(projectName, sprintId);
            if (node != null)
            {
                var nodeInfo = TfsService.GetNodeInfo(node.Uri.ToString());
                result = new Sprint()
                {
                    Id = node.Id,
                    ReleaseId = node.ParentNode != null && node.ParentNode.Id > 0 ? (int?)node.ParentNode.Id : null,
                    Name = node.Name,
                    StartDate = nodeInfo.StartDate,
                    EndDate = nodeInfo.FinishDate
                };
            }
            return result;
        }

        public Sprint GetSprintByDate(string projectName, DateTime date, int? releaseId = null)
        {
            var result = new Sprint();
            var nodes = TfsService.ListIterations(projectName);
            var currentNode = GetNodeByDate(nodes, date, releaseId);
            
            if (currentNode != null)
            {
                var nodeInfo = TfsService.GetNodeInfo(currentNode.Uri.ToString());
                result = new Sprint()
                {
                    Id = currentNode.Id,
                    ReleaseId = currentNode.ParentNode != null && currentNode.ParentNode.Id > 0 ? (int?)currentNode.ParentNode.Id : null,
                    Name = currentNode.Name,
                    StartDate = nodeInfo.StartDate,
                    EndDate = nodeInfo.FinishDate
                };
            }
            //var nodes = TfsService.ListStructures(projectName, "ProjectLifecycle");

            //if (nodes != null)
            //{
            //    return nodes
            //        .Where(w => w.StartDate <= DateTime.Now &&
            //            w.FinishDate >= DateTime.Now)
            //        .Select(w => new Sprint()
            //    {
            //        Title = w.Name
            //    }).FirstOrDefault();
            //}
            return result;
        }
        
        public Sprint GetCurrentSprint(string projectName)
        {
            return GetSprintByDate(projectName, DateTime.Now);
        }

        private Node GetNodeByDate(NodeCollection nodes, DateTime date, int? parentNodeId = null)
        {
            foreach (Node node in nodes)
            {
                if (!parentNodeId.HasValue || (parentNodeId.HasValue && node.ParentNode.Id == parentNodeId.Value))
                {
                    var nodeInfo = TfsService.GetNodeInfo(node.Uri.ToString());
                    if (nodeInfo != null && nodeInfo.StartDate <= date && nodeInfo.FinishDate >= date &&
                        !node.HasChildNodes)
                        return node;
                }
                var result = GetNodeByDate(node.ChildNodes, date, parentNodeId);
                if (result != null) return result;
                
            }
            return null;
        }
        
        private void LoadIterations(List<Iteration> list, NodeCollection collection)
        {
            foreach (Node node in collection)
            {
                var model = new Iteration();
                model.Id = node.Id;
                model.Name = node.Name;

                if (node.HasChildNodes) LoadIterations(model.SubIterations, node.ChildNodes);

                list.Add(model);
            }
        }

        public List<Project> ListProjects()
        {
            return TfsService.ListProjects().Select(p => new Project
            {
                Id = p.Id,
                Name = p.Name,
                Guid = p.Guid
            }).ToList();
        }


        #region Task
        public List<UserStoryTask> ListTasks(string projectName, string userName)
        {
            return TfsService.ListWorkItems(projectName, "Task", userName).Select(WorkItemToTask).ToList();
        }

        public List<UserStoryTask> ListTasks(string projectName, int userStoryId)
        {
            return ListTasks(projectName, userStoryId, string.Empty);
        }

        public List<UserStoryTask> ListTasks(string projectName, int userStoryId, string userName)
        {
            var result = new List<UserStoryTask>();
            IEnumerable<WorkItem> workItems;

            if (userStoryId > 0)
                workItems = TfsService.ListChildrenWorkItems(userStoryId, "Task", userName);
            else workItems = TfsService.ListWorkItems(projectName, "Task", userName);

            if (workItems != null && workItems.Any())
            {
                result = workItems.Select(WorkItemToTask).ToList();
            }
            return result;
        }

        public UserStoryTask GetTask(int taskId)
        {
            var workItem = TfsService.GetWorkItem(taskId);

            if (workItem != null)
                return WorkItemToTask(workItem);

            return null;
        }

        public void UpdateTask(UserStoryTask task)
        {
            var workItem = TfsService.GetWorkItem(task.Id);

            workItem.Fields["System.AssignedTo"].Value = task.AssignedTo;

            if (workItem.IsValid())
            {
                try
                {
                    workItem.Save();
                    return;
                }
                catch (ValidationException exception)
                {
                    throw new UpdateWorkItemException(exception);
                }
            }
            throw new UpdateWorkItemException();
        }

        public void UpdateTaskStatus(int taskId, UserStoryTaskState state)
        {
            var workItem = TfsService.GetWorkItem(taskId);
            var originalState = workItem.State;

            if (originalState == "Closed")
            {
                workItem.State = "Active";
                workItem.Save();
            }

            switch (state)
            {
                case UserStoryTaskState.ToDo:
                    workItem.State = "Active";
                    break;
                case UserStoryTaskState.Doing:
                    workItem.State = "In progress";
                    break;
                case UserStoryTaskState.Done:
                    workItem.State = "Closed";
                    break;
            }

            if (workItem.IsValid())
            {
                try
                {
                    workItem.Save();
                    return;
                }
                catch (ValidationException exception)
                {
                    throw new UpdateWorkItemException(exception);
                }
            }
            else
            {
                throw new UpdateWorkItemException("Invalid task status");
            }
        }

        public void AddTask(int projectId, UserStoryTask task)
        {
            var project = TfsService.GetProject(projectId);
            var workItemType = project.WorkItemTypes["Task"];
            var workItem = new WorkItem(workItemType)
            {
                Title = task.Title,
                Description = task.Description
            };
            workItem.Save();
        }
        #endregion


        #region Test Case
        public List<UserStoryTestCase> ListTestCases(string projectName, string userName)
        {
            return TfsService.ListWorkItems(projectName, "Test Case", userName).Select(WorkItemToTestCase).ToList();
        }

        public List<UserStoryTestCase> ListTestCases(string projectName, int userStoryId)
        {
            return ListTestCases(projectName, userStoryId, string.Empty);
        }

        public List<UserStoryTestCase> ListTestCases(string projectName, int userStoryId, string userName)
        {
            var result = new List<UserStoryTestCase>();
            IEnumerable<WorkItem> workItems;

            if (userStoryId > 0)
                workItems = TfsService.ListChildrenWorkItems(userStoryId, "Test Case", userName);
            else workItems = TfsService.ListWorkItems(projectName, "Test Case", userName);

            if (workItems != null && workItems.Any())
            {
                result = workItems.Select(WorkItemToTestCase).ToList();
            }
            return result;
        }

        public UserStoryTestCase GetTestCase(int testCaseId)
        {
            var workItem = TfsService.GetWorkItem(testCaseId);

            if (workItem != null)
                return WorkItemToTestCase(workItem);

            return null;
        }

        public void UpdateTestCaseStatus(int testCaseId, UserStoryTestCaseState state)
        {
            var workItem = TfsService.GetWorkItem(testCaseId);
            var originalState = workItem.State;

            if (originalState == "Closed")
            {
                workItem.State = "Active";
                workItem.Save();
            }

            switch (state)
            {
                case UserStoryTestCaseState.ToTest:
                    workItem.State = "Active";
                    break;
                case UserStoryTestCaseState.Testing:
                    workItem.State = "In progress";
                    break;
                case UserStoryTestCaseState.Tested:
                    workItem.State = "Closed";
                    break;
            }

            if (workItem.IsValid())
            {
                try
                {
                    workItem.Save();
                    return;
                }
                catch (ValidationException exception)
                {
                    throw new UpdateWorkItemException(exception);
                }
            }
            else
            {
                throw new UpdateWorkItemException("Invalid test case status");
            }
        }
        #endregion


        #region User Story
        public List<UserStory> ListUserStories(string projectName, int iterationId, string teamName, bool withTasks = false)
        {
            var userStories = new List<UserStory>();
            var workItems = TfsService.ListWorkItems(projectName, "User Story", iterationId, string.Empty,
                new Dictionary<string, string>{ {"Team", teamName} });

            if (workItems != null && workItems.Count() > 0)
            {
                userStories = workItems.Select(o =>
                {
                    return new UserStory()
                    {
                        Id = o.Id,
                        Description = o.Description,
                        Title = o.Title,
                        State = o.State,
                        AssignedTo = new User() {DisplayName = o.Fields["System.AssignedTo"].Value.ToString()},
                        Tasks = withTasks ? ListTasks(projectName, o.Id) : new List<UserStoryTask>()
                    };
                }).ToList();
            }
            return userStories;
        }

        public void UpdateUserStoryStatus(int userStoryId, UserStoryState state)
        {
            var workItem = TfsService.GetWorkItem(userStoryId);
            var originalState = workItem.State;


            if (originalState == "Closed")
            {
                workItem.State = "Active";
                workItem.Save();
            }

            switch (state)
            {
                case UserStoryState.Accepted:
                    workItem.State = "Active";
                    break;
                case UserStoryState.Available:
                    workItem.State = "Active";
                    break;
                case UserStoryState.Delivered:
                    workItem.State = "Resolved";
                    break;
                case UserStoryState.Rejected:
                    workItem.State = "Closed";
                    break;
            }

            if (workItem.IsValid())
            {
                try
                {
                    workItem.Save();
                    return;
                }
                catch (ValidationException exception)
                {
                    throw new UpdateWorkItemException(exception);
                }
            }
            else
            {
                throw new UpdateWorkItemException("Invalid user story status");
            }
        }
        #endregion


        #region Helpers
        private UserStoryTask WorkItemToTask(WorkItem workItem)
        {
            var displayName = workItem.Fields["System.AssignedTo"].Value.ToString();
            var user = GetUserInformation(displayName);

            return new UserStoryTask()
            {
                Id = workItem.Id,
                Description = workItem.Description,
                Title = workItem.Title,
                State = workItem.State,
                AssignedTo = user,
                ProjectName = workItem.Project.Name
            };
        }

        private UserStoryTestCase WorkItemToTestCase(WorkItem workItem)
        {
            var displayName = workItem.Fields["System.AssignedTo"].Value.ToString();
            var user = GetUserInformation(displayName);

            return new UserStoryTestCase()
            {
                Id = workItem.Id,
                Description = workItem.Description,
                Title = workItem.Title,
                State = workItem.State,
                AssignedTo = user,
                ProjectName = workItem.Project.Name
            };
        }

        private User GetUserInformation(string displayName)
        {
            var user = new User() { DisplayName = displayName };
            var userRead = TfsService.GetIdentity(displayName);
            if (userRead != null)
                user = TeamFoundationIdentityToUser(userRead);
            return user;
        }

        private User TeamFoundationIdentityToUser(TeamFoundationIdentity identity)
        {
            object imageId = null;
            object email = string.Empty;
            identity.TryGetProperty("Mail", out email);
            identity.TryGetProperty("Microsoft.TeamFoundation.Identity.Image.Id", out imageId);

            return new User
            {
                Id = identity.TeamFoundationId,
                DisplayName = identity.DisplayName,
                UserName = identity.UniqueName,
                Email = email.ToString(),
                ImageId = imageId == null ? null : (Guid?)new Guid((byte[])imageId)
            };
        }
        #endregion
    }
}
