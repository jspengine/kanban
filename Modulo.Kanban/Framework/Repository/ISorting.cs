﻿namespace Modulo.Kanban.Framework.Repository
{
    public interface ISorting
    {
        string ColumnName { get; set; }
        bool Ascending { get; set; }
    }
}
