﻿namespace Modulo.Kanban.Framework.Repository
{
    public class Sorting : ISorting
    {
        public string ColumnName { get; set; }
        public bool Ascending { get; set; }

        public Sorting()
        {
            Ascending = true;
        }
    }
}
