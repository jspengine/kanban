﻿using System;
using System.Linq.Expressions;

namespace Modulo.Kanban.Framework.Specification
{
    public interface ISpecification<TEntity> where TEntity :class
    {
        Expression<Func<TEntity, bool>> SatisfiedBy();
    }
}
