﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Framework.Repository;
using Modulo.Kanban.Integration.Repository.Contracts;

namespace Modulo.Kanban.Integration.Repository
{
    public class StateRepository : Repository<State, int>, IStateRepository
    {
        public StateRepository(IQueryableUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
