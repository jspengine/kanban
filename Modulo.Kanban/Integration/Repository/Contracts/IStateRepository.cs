﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Framework.Repository;

namespace Modulo.Kanban.Integration.Repository.Contracts
{
    public interface IStateRepository : IRepository<State, int>
    {
    }
}
