﻿using System;
using System.Collections.Generic;

using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Modulo.Kanban.Framework.Repository;
using Modulo.Kanban.Integration.Migrations;

namespace Modulo.Kanban.Integration
{
    public class MainUnitOfWork : DbContext, IQueryableUnitOfWork
    {
        private const string LocalConnectionStringName = "DefaultConnection";
        private readonly string _connectionString;

        public static int IsMigrating = 0;

        public MainUnitOfWork()
            : base(LocalConnectionStringName)
        {
            this.Configuration.LazyLoadingEnabled = true;
        }


        public int Commit()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CommitAndRefreshChanges()
        {
            int changes = 0;
            bool saveFailed = false;
            
            do
            {
                try
                {
                    changes = base.SaveChanges();

                    saveFailed = false;

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;

                    ex.Entries.ToList()
                              .ForEach(entry => entry.OriginalValues.SetValues(entry.GetDatabaseValues()));

                }
                catch (DbEntityValidationException e)
                {
                    string x = e.Message;
                }
            } while (saveFailed);

            return changes;
        }

        public void RollbackChanges()
        {
            // set all entities in change tracker 
            // as 'unchanged state'
            base.ChangeTracker.Entries()
                .ToList()
                .ForEach(entry => entry.State = EntityState.Unchanged);
        }

        public IEnumerable<TEntity> ExecuteQuery<TEntity>(string sqlQuery, params object[] parameters)
        {
            return base.Database.SqlQuery<TEntity>(sqlQuery, parameters);
        }

        public int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return base.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }

        public IDbSet<TEntity> CreateSet<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void Attach<TEntity>(TEntity item) where TEntity : class
        {
            //attach and set as unchanged
            base.Entry<TEntity>(item).State = EntityState.Unchanged;
        }

        public void SetModified<TEntity>(TEntity item) where TEntity : class
        {
            //this operation also attach item in object state manager
            base.Entry<TEntity>(item).State = EntityState.Modified;
        }

        public void ApplyCurrentValues<TEntity>(TEntity original, TEntity current) where TEntity : class
        {
            //if it is not attached, attach original and set current values
            base.Entry<TEntity>(original).CurrentValues.SetValues(current);
        }

        public void LoadCollection<TEntity, TElement>(TEntity item, Expression<Func<TEntity, ICollection<TElement>>> navigationProperty) where TEntity : class where TElement : class
        {
            base.Entry<TEntity>(item).Collection(navigationProperty).Load();
        }

        public void LoadCollection<TEntity, TElement>(TEntity item, Expression<Func<TEntity, ICollection<TElement>>> navigationProperty, Expression<Func<TEntity, bool>> filter) where TEntity : class where TElement : class
        {
            throw new NotImplementedException();
        }

        public void LoadProperty<TEntity, TComplexProperty>(TEntity item, Expression<Func<TEntity, TComplexProperty>> selector) where TEntity : class where TComplexProperty : class
        {
            throw new NotImplementedException();
        }

        public void UpdateDatabase()
        {
            if (0 == System.Threading.Interlocked.Exchange(ref IsMigrating, 1))
            {
                try
                {
                    var dbMigrator = GetMigrator();
                    if (dbMigrator.GetPendingMigrations().Any())
                    {
                        dbMigrator.Update();
                    }
                }
                finally
                {
                    System.Threading.Interlocked.Exchange(ref IsMigrating, 0);
                }
            }
        }

        public IEnumerable<string> GetPendingMigrations()
        {
            var dbMigrator = GetMigrator();
            return dbMigrator.GetPendingMigrations();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new ProcessMapping());
        }

        private DbMigrator GetMigrator()
        {
            var config = new Configuration();
            config.TargetDatabase = new DbConnectionInfo(_connectionString);
            return new DbMigrator(config);
        }
    }
}
