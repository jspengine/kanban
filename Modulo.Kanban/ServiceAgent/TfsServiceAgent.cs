﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Principal;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Framework.Client;
using Microsoft.TeamFoundation.Framework.Common;
using Microsoft.TeamFoundation.Server;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Modulo.Kanban.Domain.Entities;
using Modulo.Kanban.Domain.Exceptions;
using Project = Microsoft.TeamFoundation.WorkItemTracking.Client.Project;

namespace Modulo.Kanban.ServiceAgent
{
    public class TfsServiceAgent
    {
        private readonly TfsTeamProjectCollection _temProjectCollection;
        private readonly string _tfsUrl;
        public TfsServiceAgent()
        {
            _tfsUrl = ConfigurationManager.AppSettings["TfsUrl"];
            var tfsUri = new Uri(_tfsUrl);
            _temProjectCollection = new TfsTeamProjectCollection(tfsUri);
        }

        public TeamProject GetTeamProjectByName(string projectName)
        {
            var vsStore = _temProjectCollection.GetService<VersionControlServer>();
            return vsStore.TryGetTeamProject(projectName);
        }

        public Project GetProjectByName(string projectName)
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            return workItemStore.Projects[projectName];
        }

        public Project GetProject(int projectId)
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            return workItemStore.Projects.GetById(projectId);
        }

        public List<Project> ListProjects()
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            return workItemStore.Projects.Cast<Microsoft.TeamFoundation.WorkItemTracking.Client.Project>().ToList();
        }

        public IEnumerable<WorkItem> ListWorkItems(string projectName, string workItemType, string assignedTo)
        {
            return ListWorkItems(projectName, workItemType, 0, assignedTo, null);
        }

        public IEnumerable<WorkItem> ListWorkItems(string projectName, string workItemType, int iterationId, string assignedTo, Dictionary<string, string> customFields)
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();


            var parameters = new Dictionary<string, object>();
            parameters.Add("Project", projectName);
            parameters.Add("AssignedTo", assignedTo);
            parameters.Add("WorkItemType", workItemType);
            parameters.Add("Iteration", iterationId);

            string queryStr = @"
                SELECT  [System.Id],
                        [System.Title],
                        [System.AssignedTo]

                FROM    WorkItems 

                WHERE   [System.WorkItemType] = @WorkItemType AND
                        [System.TeamProject] = @Project ";

            if (iterationId > 0)
            {
                queryStr += "AND [System.IterationId] = @Iteration ";
            }
            if (customFields != null && customFields.Count > 0)
            {
                foreach (var kvp in customFields)
                {
                    if (!string.IsNullOrWhiteSpace(kvp.Value))
                    {
                        parameters.Add(kvp.Key, kvp.Value);
                        queryStr += string.Format("AND [{0}] = @{0} ", kvp.Key);
                    }
                }
            }
            
            if (!string.IsNullOrWhiteSpace(assignedTo))
                queryStr += " AND [System.AssignedTo] = @AssignedTo ";

            Query query = new Query(workItemStore, queryStr, parameters);
            return workItemStore.Query(query.QueryString).Cast<WorkItem>();
        }

        public IEnumerable<WorkItem> ListChildrenWorkItems(int parentWorkItemId, string childWorkItemType,
            string assignetTo)
        {
            IEnumerable<WorkItem> workItems = new List<WorkItem>();
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            var parameters = new Dictionary<string, object>();
            parameters.Add("AssignedTo", assignetTo);
            parameters.Add("Id", parentWorkItemId);
            parameters.Add("ChildWorkItemType", childWorkItemType);

            string queryStr = @"
                SELECT  [System.Id]
                FROM    WorkItemLinks
                WHERE   [Source].[System.Id] = @Id AND
                        [System.Links.LinkType] = 'Child' AND
                        [Target].[System.WorkItemType] = @ChildWorkItemType ";

            if (!string.IsNullOrWhiteSpace(assignetTo))
                queryStr += "AND [Target].[System.AssignedTo] = @AssignedTo ";

            queryStr += "ORDER BY [System.Id] mode(MustContain)";

            Query query = new Query(workItemStore, queryStr, parameters);

            var wiTrees = query.RunLinkQuery();
            var ids = wiTrees.Where(o => o.TargetId != parentWorkItemId).Select(o => o.TargetId);
            if (ids.Count() > 0)
            {
                Query query2 = new Query(workItemStore, @"
                SELECT  [System.Id],
                        [System.Title],
                        [System.AssignedTo]

                FROM    WorkItems 

                WHERE   [System.Id] IN (" + string.Join(",", ids) + ")");

                workItems = workItemStore.Query(query2.QueryString).Cast<WorkItem>();
            }

            return workItems;
        }

        public NodeCollection ListIterations(int projectId)
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            var project = workItemStore.Projects.GetById(projectId);
            return project.IterationRootNodes;
        }

        public Node GetIteration(string projectName, int iterationId)
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            var project = workItemStore.Projects[projectName];
            return project.FindNodeInSubTree(iterationId);
        }

        public NodeCollection ListIterations(string projectName)
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            var project = workItemStore.Projects[projectName];
            return project.IterationRootNodes;
        }

        public NodeInfo GetNodeInfo(string nodeUri)
        {
            var css = _temProjectCollection.GetService<ICommonStructureService4>();
            return css.GetNode(nodeUri);

            //return nodes.FirstOrDefault();
        }

        public IEnumerable<NodeInfo> ListStructures(string projectName, string structureType)
        {
            var css = _temProjectCollection.GetService<ICommonStructureService4>();
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            var project = workItemStore.Projects[projectName];

            return css.ListStructures(project.Uri.ToString()).Where(s => s.StructureType == structureType);
        }
        public IEnumerable<string> ListFieldValues(string name)
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            var field = workItemStore.FieldDefinitions.TryGetByName(name);
            return field.AllowedValues.Cast<string>();
        }

        public WorkItem GetWorkItem(int workItemId)
        {
            var workItemStore = _temProjectCollection.GetService<WorkItemStore>();
            return workItemStore.GetWorkItem(workItemId);
        }

        public TeamFoundationIdentity GetIdentity(string displayName)
        {
            var identityService = _temProjectCollection.GetService<IIdentityManagementService2>();
            

            if (!string.IsNullOrWhiteSpace(displayName))
            {
                var fetchProperties = new string[]
                {
                    "Microsoft.TeamFoundation.Identity.Image.Id",
                    "Microsoft.TeamFoundation.Identity.Image.Data",
                    "Microsoft.TeamFoundation.Identity.Image.Type"
                };
                var userRead = identityService.ReadIdentity(IdentitySearchFactor.DisplayName, displayName,
                    MembershipQuery.Direct, ReadIdentityOptions.ExtendedProperties, fetchProperties,
                    IdentityPropertyScope.Both);

                return userRead;
            }

            return null;
        }

        public List<TeamFoundationIdentity> ListIdentities(string projectName)
        {
            var css4 = _temProjectCollection.GetService<ICommonStructureService4>();
            var projectInfo = css4.GetProjectFromName(projectName);
            var teamService = _temProjectCollection.GetService<TfsTeamService>();
            var allTeams = teamService.QueryTeams(projectInfo.Uri);
            var result = new List<TeamFoundationIdentity>();

            foreach (TeamFoundationTeam team in allTeams)
            {
                var members = team.GetMembers(_temProjectCollection, MembershipQuery.Direct);
                var users = members.Where(m => !m.IsContainer);
                foreach (var user in users)
                {
                    if(!result.Select(o => o.UniqueName).Contains(user.UniqueName)) result.Add(user);
                }
            }
            return result.OrderBy(o => o.DisplayName).ToList();
        }
    }
}